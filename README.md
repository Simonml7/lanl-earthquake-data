This repository contains training and submission data for the [LANL earthquake prediction on kaggle.com](https://www.kaggle.com/c/LANL-Earthquake-Prediction).

All the data is stored in .npy files, because I use numpy arrays.
There are 3 files:
1.  train_data_124.npy => a numpy array (12288 x 124) => It stores 124 statistical features of 12288 segments, which usually consist of 150,000 acoustic signals but are broken down
    to those features so they are easier to learn. More infos about the features can be found in the report.
2.  train_label_124.npy => a numpy array (12288) => It stores the time to the next earthquake and it is used for the labels.
3.  submission_data124.npy => a numpy array (2624 x 124) => It stores the statistical features of the segments that are used for the submission.